import axios from 'axios';
import {Table, TableContainer, TableHead, TableCell, TableBody, TableRow} from '@material-ui/core';
import React, {useEffect, useState} from 'react';



const baseUrl='https://altokpe.herokuapp.com/altokePe/usuario/listar/1'

function Historial(){

    const [data, setData]=useState([]);

    

    const peticionGet=async()=>{
        await axios.get(baseUrl)
        .then(response=>{
            setData(response.data);
        })
    }

    useEffect(async()=>{
        await peticionGet();
    }, [])


    return (
        
        <div className="Cuenta">
            <br/>
            <br/><br/>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>idUsuario</TableCell>                                                                                
                            
                            <TableCell>Nombres</TableCell>
                            <TableCell>Apellidos</TableCell>
                            <TableCell>Dni</TableCell>
                            <TableCell>Correo</TableCell>
                            <TableCell>Dinero</TableCell>                                              
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {data.map(consola=>(
                            <TableRow key={consola.idusuario}>                            
                                <TableCell>{consola.idusuario}</TableCell>
                                <TableCell>{consola.nombres}</TableCell>
                                <TableCell>{consola.apellidos}</TableCell>
                                <TableCell>{consola.dni}</TableCell>                                
                                <TableCell>{consola.correo}</TableCell>
                                <TableCell>{consola.dinero}</TableCell>                                       
                            </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>            
        </div>
    
    );

}

export default Historial;