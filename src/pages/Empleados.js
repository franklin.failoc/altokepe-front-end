import axios from 'axios';
import {Table, TableContainer, TableHead, TableCell, TableBody, TableRow, Modal, Button, TextField} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {Edit, Delete} from '@material-ui/icons';
import {makeStyles} from '@material-ui/core/styles';


const baseUrl='https://altokpe.herokuapp.com/altokePe/empleados/listar/1'
const baseUrl2='https://altokpe.herokuapp.com/altokePe/empleado/insertar/'
const baseUrl3= 'https://altokpe.herokuapp.com/altokePe/empleado/eliminar/'
const baseUrl4= 'https://altokpe.herokuapp.com/altokePe/empleado/actualizar'




const useStyles = makeStyles((theme) => ({
    modal: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    iconos:{
      cursor: 'pointer'
    }, 
    inputMaterial:{
      width: '100%'
    }
}));

function Empleados(){
    const styles= useStyles();

    const [data, setData]=useState([]);
    const [modalInsertar,  setModalInsertar]=useState(false);
    const [modalEditar, setModalEditar]=useState(false);
    const [modalEliminar, setModalEliminar]=useState(false);

    const [consolaSeleccionada, setConsolaSeleccionada]=useState({
        idempleado: '',
        nombres: '',
        apellidos: '',
        dni: '',
        celular: '',
        correo: '',
        ocupacion: '',
        sueldo: '',
        fechainicio: '',
        fechafin: ''
    })
    
    const handleChange=e=>{
        const {name, value}=e.target;
        setConsolaSeleccionada(prevState=>({
            ...prevState,
            [name]: value
        }))
        console.log(consolaSeleccionada);
    }

    const peticionGet=async()=>{
        await axios.get(baseUrl)
        .then(response=>{
            setData(response.data);
        })
    }

    

    const peticionPost=async()=>{
        await axios.post(baseUrl2, consolaSeleccionada)
        .then(response=>{
            setData(data.concat(response.data))
            abrirCerrarModalInsertar()
        })
        peticionGet();
    }

    const peticionPut=async()=>{
        await axios.post(baseUrl4, consolaSeleccionada)
        .then(response=>{
            var dataNueva=data;
            dataNueva.map(consola=>{
                if(consolaSeleccionada.idempleado===consola.idempleado){
                    consola.nombres=consolaSeleccionada.nombres;
                    consola.apellidos=consolaSeleccionada.apellidos;
                    consola.dni=consolaSeleccionada.dni;
                    consola.celular=consolaSeleccionada.celular;
                    consola.correo=consolaSeleccionada.correo;
                    consola.ocupacion=consolaSeleccionada.ocupacion;
                    consola.sueldo=consolaSeleccionada.sueldo;
                    consola.fechainicio=consolaSeleccionada.fechainicio;
                    consola.fechafin=consolaSeleccionada.fechafin
                }
            })
            setData(dataNueva);
            abrirCerrarModalEditar();
        })
        peticionGet()
    }

    

    const peticionDelete=async()=>{
        await axios.get(baseUrl3+consolaSeleccionada.idempleado)
        .then(response=>{
            setData(data.filter(consola=>consola.idempleado!==consolaSeleccionada.idempleado));
            abrirCerrarModalEliminar();
        })
        peticionGet()
    }

    const abrirCerrarModalInsertar=()=>{
        setModalInsertar(!modalInsertar);
    }

    const abrirCerrarModalEditar=()=>{
        setModalEditar(!modalEditar);
    }

    const abrirCerrarModalEliminar=()=>{
        setModalEliminar(!modalEliminar);
    }

    const seleccionarConsola=(consola, caso)=>{
        setConsolaSeleccionada(consola);
        (caso==='Editar')?abrirCerrarModalEditar():abrirCerrarModalEliminar()
    }

    useEffect(async()=>{
        await peticionGet();
    }, [])

    const bodyInsertar=(
        <div className={styles.modal}>
            <h3>Agregar Nuevo Empleado</h3>
            <TextField name="nombres" className={styles.inputMaterial} label="Nombre" onChange={handleChange}/><br/>
            <TextField name="apellidos" className={styles.inputMaterial} label="Apellidos" onChange={handleChange}/><br/>
            <TextField name="dni" className={styles.inputMaterial} label="DNI" onChange={handleChange}/><br/>
            <TextField name="celular" className={styles.inputMaterial} label="Celular" onChange={handleChange}/><br/>
            <TextField name="correo" className={styles.inputMaterial} label="Correo" onChange={handleChange}/><br/>
            <TextField name="ocupacion" className={styles.inputMaterial} label="Ocupacion" onChange={handleChange}/><br/>
            <TextField name="sueldo" className={styles.inputMaterial} label="Sueldo" onChange={handleChange}/><br/>
            <TextField name="fechainicio" className={styles.inputMaterial} label="Fecha De Inicio" onChange={handleChange}/><br/>
            <TextField name="fechafin" className={styles.inputMaterial} label="Finaliza" onChange={handleChange}/><br/>
            <br/><br/>
            <div align="right">
                <Button color="primary" onClick={()=>peticionPost()}>Insertar</Button>
                <Button onClick={()=>abrirCerrarModalInsertar()}>Cancelar</Button>
            </div>
        </div>
    )

    const bodyEditar=(
        <div className={styles.modal}>
            <h3>Editar Usuario</h3>
            <TextField name="idempleado" className={styles.inputMaterial} readOnly label="IdEmpleado" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.idempleado} disabled /><br/>

            <TextField name="nombres" className={styles.inputMaterial} label="Nombre" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.nombres}/><br/>

            <TextField name="apellidos" className={styles.inputMaterial} label="Apellidos" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.apellidos}/><br/>

            <TextField name="dni" className={styles.inputMaterial} label="DNI" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.dni}/><br/>

            <TextField name="celular" className={styles.inputMaterial} label="Celular" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.celular}/><br/>

            <TextField name="correo" className={styles.inputMaterial} label="Correo" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.correo}/><br/>

            <TextField name="ocupacion" className={styles.inputMaterial} label="Ocupacion" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.ocupacion}/><br/>

            <TextField name="sueldo" className={styles.inputMaterial} label="Sueldo" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.sueldo}/><br/>

            <TextField name="fechainicio" className={styles.inputMaterial} label="Fecha De Inicio" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.fechainicio}/><br/>

            <TextField name="fechafin" className={styles.inputMaterial} label="Finaliza" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.fechafin}/><br/>
            <br/><br/>
            <div align="right">
                <Button color="primary" onClick={()=>peticionPut()}>Editar</Button>
                <Button onClick={()=>abrirCerrarModalEditar()}>Cancelar</Button>
            </div>
        </div>
    )

    const bodyEliminar=(
        <div className={styles.modal}>
            <p>¿Estas seguro que deseas eliminar este usuario &nbsp;
            <b>{consolaSeleccionada && consolaSeleccionada.nombres}</b>?</p>
            
            <div align="right">
                <Button color="secondary" onClick={()=>peticionDelete()}>Si</Button>
                <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>
            </div>
        </div>
    )

    return (
        
        <div className="Cuenta">
            <br/>
            <Button onClick={()=>abrirCerrarModalInsertar()}>Insertar</Button>
            <br/><br/>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>idempleado</TableCell>                                                           
                            <TableCell>Nombres</TableCell>
                            <TableCell>Apellidos</TableCell>
                            <TableCell>DNI</TableCell>
                            <TableCell>Celular</TableCell>
                            <TableCell>Correo</TableCell>
                            <TableCell>Ocupacion</TableCell>
                            <TableCell>Sueldo</TableCell>
                            <TableCell>Inicia</TableCell>
                            <TableCell>Finaliza</TableCell>
                            <TableCell>Estado</TableCell>
                            <TableCell>Acciones</TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {data.map(consola=>(
                            <TableRow key={consola.idempleado}>                            
                                <TableCell>{consola.idempleado}</TableCell>                                
                                <TableCell>{consola.nombres}</TableCell>
                                <TableCell>{consola.apellidos}</TableCell>
                                <TableCell>{consola.dni}</TableCell>
                                <TableCell>{consola.celular}</TableCell>
                                <TableCell>{consola.correo}</TableCell>
                                <TableCell>{consola.ocupacion}</TableCell>                                   
                                <TableCell>{consola.sueldo}</TableCell>
                                <TableCell>{consola.fechainicio}</TableCell>
                                <TableCell>{consola.fechafin}</TableCell>
                                <TableCell>{consola.estado}</TableCell>
                                <TableCell>
                                    <Edit className={styles.iconos} onClick={()=>seleccionarConsola(consola, 'Editar')} /> 
                                    &nbsp;&nbsp;&nbsp;
                                    <Delete className={styles.iconos} onClick={()=>seleccionarConsola(consola, 'Eliminar')}/>
                                </TableCell>
                            </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <Modal
                open={modalInsertar}
                onClose={abrirCerrarModalInsertar}>
                    {bodyInsertar}
            </Modal>

            <Modal
                open={modalEditar}
                onClose={abrirCerrarModalEditar}>
                    {bodyEditar}
            </Modal>

            <Modal
                open={modalEliminar}
                onClose={abrirCerrarModalEliminar}>
                    {bodyEliminar}
            </Modal>

            
        </div>
    
    );

}

export default Empleados;