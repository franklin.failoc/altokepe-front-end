
import axios from 'axios';
import {Table, TableContainer, TableHead, TableCell, TableBody, TableRow, Modal, Button, TextField} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {Edit, Delete} from '@material-ui/icons';
import {makeStyles} from '@material-ui/core/styles';

const baseUrl='http://127.0.0.1:8000/producto/'

const useStyles = makeStyles((theme) => ({
    modal: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    iconos:{
      cursor: 'pointer'
    }, 
    inputMaterial:{
      width: '100%'
    }
}));

function Cuenta() {
    const styles= useStyles();

    const [data, setData]=useState([]);
    const [modalInsertar,  setModalInsertar]=useState(false);
    const [modalEditar, setModalEditar]=useState(false);
    const [modalEliminar, setModalEliminar]=useState(false);

    const [consolaSeleccionada, setConsolaSeleccionada]=useState({
        nombre: '',
        descripcion: '',
        precio: ''
    })

    const handleChange=e=>{
        const {name, value}=e.target;
        setConsolaSeleccionada(prevState=>({
            ...prevState,
            [name]: value
        }))
        console.log(consolaSeleccionada);
    }

    const peticionGet=async()=>{
        await axios.get(baseUrl)
        .then(response=>{
            setData(response.data);
        })
    }

    const peticionPost=async()=>{
        await axios.post(baseUrl, consolaSeleccionada)
        .then(response=>{
            setData(data.concat(response.data))
            abrirCerrarModalInsertar()
        })
    }

    const peticionPut=async()=>{
        await axios.put(baseUrl+consolaSeleccionada.id, consolaSeleccionada)
        .then(response=>{
            var dataNueva=data;
            dataNueva.map(consola=>{
                if(consolaSeleccionada.id===consola.id){
                    consola.nombre=consolaSeleccionada.nombre;
                    consola.descripcion=consolaSeleccionada.descripcion;
                    consola.precio=consolaSeleccionada.precio;
                }
            })
            setData(dataNueva);
            abrirCerrarModalEditar();
        })
    }

    const peticionDelete=async()=>{
        await axios.delete(baseUrl+consolaSeleccionada.id)
        .then(response=>{
            setData(data.filter(consola=>consola.id!==consolaSeleccionada.id));
            abrirCerrarModalEliminar();
        })
    }

    const abrirCerrarModalInsertar=()=>{
        setModalInsertar(!modalInsertar);
    }

    const abrirCerrarModalEditar=()=>{
        setModalEditar(!modalEditar);
    }

    const abrirCerrarModalEliminar=()=>{
        setModalEliminar(!modalEliminar);
    }

    const seleccionarConsola=(consola, caso)=>{
        setConsolaSeleccionada(consola);
        (caso==='Editar')?abrirCerrarModalEditar():abrirCerrarModalEliminar()
    }
    useEffect(async()=>{
        await peticionGet();
    }, [])

    const bodyInsertar=(
        <div className={styles.modal}>
            <h3>Agregar Nuevo Producto</h3>
            <TextField name="nombre" className={styles.inputMaterial} label="Nombre" onChange={handleChange}/><br/>
            <TextField name="descripcion" className={styles.inputMaterial} label="Descripcion" onChange={handleChange}/><br/>
            <TextField name="precio" className={styles.inputMaterial} label="Precio" onChange={handleChange}/><br/>
            <br/><br/>
            <div align="right">
                <Button color="primary" onClick={()=>peticionPost()}>Insertar</Button>
                <Button onClick={()=>abrirCerrarModalInsertar()}>Cancelar</Button>
            </div>
        </div>
    )

    const bodyEditar=(
        <div className={styles.modal}>
            <h3>Editar Producto</h3>
            <TextField name="nombre" className={styles.inputMaterial} label="Nombre" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.nombre}/><br/>
            <TextField name="descripcion" className={styles.inputMaterial} label="Descripcion" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.descripcion}/><br/>
            <TextField name="precio" className={styles.inputMaterial} label="Precio" onChange={handleChange} value={consolaSeleccionada && consolaSeleccionada.precio}/>
            <br/><br/>
            <div align="right">
                <Button color="primary" onClick={()=>peticionPut()}>Editar</Button>
                <Button onClick={()=>abrirCerrarModalEditar()}>Cancelar</Button>
            </div>
        </div>
    )

    const bodyEliminar=(
        <div className={styles.modal}>
            <p>¿Estas seguro que deseas eliminar el producto
            <b>{consolaSeleccionada && consolaSeleccionada.nombre}</b>?</p>
            
            <div align="right">
                <Button color="secondary" onClick={()=>peticionDelete()}>Si</Button>
                <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>
            </div>
        </div>
    )

    return (
        
            <div className="Cuenta">
                <br/>
                <Button onClick={()=>abrirCerrarModalInsertar()}>Insertar</Button>
                <br/><br/>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Codigo</TableCell>                                                             
                                <TableCell>Nombre</TableCell>
                                <TableCell>Descripcion</TableCell>
                                <TableCell>Precio</TableCell>
                                <TableCell>Acciones</TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {data.map(consola=>(
                                <TableRow key={consola.id}>
                                    <TableCell>{consola.id}</TableCell>                                   
                                    <TableCell>{consola.nombre}</TableCell>
                                    <TableCell>{consola.descripcion}</TableCell>
                                    <TableCell>{consola.precio}</TableCell>
                                    <TableCell>
                                        <Edit className={styles.iconos} onClick={()=>seleccionarConsola(consola, 'Editar')}/> 
                                        &nbsp;&nbsp;&nbsp;
                                        <Delete className={styles.iconos} onClick={()=>seleccionarConsola(consola, 'Eliminar')}/>
                                    </TableCell>
                                </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                <Modal
                    open={modalInsertar}
                    onClose={abrirCerrarModalInsertar}>
                        {bodyInsertar}
                </Modal>

                <Modal
                    open={modalEditar}
                    onClose={abrirCerrarModalEditar}>
                        {bodyEditar}
                </Modal>

                <Modal
                    open={modalEliminar}
                    onClose={abrirCerrarModalEliminar}>
                        {bodyEliminar}
                </Modal>
            </div>
        
    );
}


export default Cuenta;