import axios from 'axios';
import {Table, TableContainer, TableHead, TableCell, TableBody, TableRow} from '@material-ui/core';
import React, {useEffect, useState} from 'react';



const baseUrl='https://altokpe.herokuapp.com/altokePe/pedidos/listar/1'



function Ventas(){
    

    const [data, setData]=useState([]);

    

    const peticionGet=async()=>{
        await axios.get(baseUrl)
        .then(response=>{
            setData(response.data);
        })
    }

    useEffect(async()=>{
        await peticionGet();
    }, [])


    return (
        
        <div className="Cuenta">
            <br/>
            <br/><br/>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>idPedido</TableCell>                                                                                
                            
                            <TableCell>Nombres</TableCell>
                            <TableCell>Apellidos</TableCell>
                            <TableCell>Menu</TableCell>
                            <TableCell>Descripcion</TableCell>
                            <TableCell>Precio</TableCell>
                            <TableCell>Cantidad</TableCell>
                            <TableCell>Direccion</TableCell>
                            <TableCell>FMenu</TableCell>
                            <TableCell>Hora</TableCell>
                            <TableCell>Estado</TableCell>                        
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {data.map(consola=>(
                            <TableRow key={consola.idpedido}>                            
                                <TableCell>{consola.idpedido}</TableCell>
                                <TableCell>{consola.nombres}</TableCell>
                                <TableCell>{consola.apellidos}</TableCell>
                                <TableCell>{consola.nombre}</TableCell>
                                <TableCell>{consola.descripcion}</TableCell>
                                <TableCell>{consola.precio}</TableCell>
                                <TableCell>{consola.cantidad}</TableCell>
                                <TableCell>{consola.direccion}</TableCell>                                   
                                <TableCell>{consola.fechamenu}</TableCell>
                                <TableCell>{consola.hora}</TableCell>
                                <TableCell>{consola.estado}</TableCell>        
                            </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>            
        </div>
    
    );

}

export default Ventas;