import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Dashboard from './Dashboard';
import login from './login';
import { watchUserChanges } from './servicios/firebase/watcher';
import 'bootstrap/scss/bootstrap.scss';
import GuardRoute from './seguridadlogin/guardRoute';
import { AuthContextProvider } from './context/auth';
import Root from './seguridadlogin/root';


watchUserChanges((user) => {
    console.log(user);
})

const root =(
    <BrowserRouter>
      <AuthContextProvider>
        <Root>
          <Switch>
            <GuardRoute type="public" path="/login" component={login}/>
            <GuardRoute type="private" path="/Dashboard" component={Dashboard}/>
            <Redirect from="/" to="/Dashboard"/>
          </Switch>
        </Root>
      </AuthContextProvider>
    </BrowserRouter>
);

ReactDOM.render(root, document.getElementById('root'));


