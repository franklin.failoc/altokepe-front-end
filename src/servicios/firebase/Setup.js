import firebase from 'firebase';
import firebaseui from 'firebaseui';


const firebaseConfig = {
    apiKey: "AIzaSyBnssQqaq_YXFuG05J6Ja_6awJkc5HMBiU",
    authDomain: "fir-4da49.firebaseapp.com",
    databaseURL: "https://fir-4da49-default-rtdb.firebaseio.com",
    projectId: "fir-4da49",
    storageBucket: "fir-4da49.appspot.com",
    messagingSenderId: "673395613316",
    appId: "1:673395613316:web:dc45b70810d7cfd5a72cd1",
    measurementId: "G-07FQHB6WVE"
};

const uiConfig = {
    signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        // firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        // firebase.auth.GithubAuthProvider.PROVIDER_ID,
        // firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    signInSuccessUrl: '/',
};

firebase.initializeApp(firebaseConfig);


export const auth = firebase.auth();

export const startUi = (elementId) => {
    const ui = new firebaseui.auth.AuthUI(auth);
    ui.start(elementId, uiConfig);
};
