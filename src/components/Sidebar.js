import { NavLink } from 'react-router-dom';
import * as FaIcons from 'react-icons/fa';
import * as GiIcons from "react-icons/gi";
const Sidebar = () =>{
    return(
        <div className="sidebar">
            <ul>
                <li>
                    <NavLink to="/" exact className="text-white rounder py-2 w-100 d-inline-block px-3" activeClassName="active"><FaIcons.FaUserCircle className="me-2"/> Cuenta</NavLink>                    
                </li>
                <li>
                    <NavLink to="/menus" exact className="text-white rounder py-2 w-100 d-inline-block px-3" activeClassName="active"><GiIcons.GiCampCookingPot className="me-2"/>Menus</NavLink>                    
                </li>
                
                <li>
                    <NavLink to="/ventas" exact className="text-white rounder py-2 w-100 d-inline-block px-3" activeClassName="active"><FaIcons.FaRegChartBar className="me-2"/>Ventas</NavLink>                    
                </li>
                <li>
                    <NavLink to="/clientes" exact className="text-white rounder py-2 w-100 d-inline-block px-3" activeClassName="active"><FaIcons.FaUserFriends className="me-2"/>Clientes</NavLink>                    
                </li>                
                <li>
                    <NavLink to="/empleados" exact className="text-white rounder py-2 w-100 d-inline-block px-3" activeClassName="active"><FaIcons.FaUserFriends className="me-2"/>Empleados</NavLink>                    
                </li>
            </ul>
        </div>
    )
}

export default Sidebar;