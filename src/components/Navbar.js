import React from 'react';
import logo from "../logo.png";


const Navbar = () => {
  
  return (
    <nav className="navbar bg-dark">
      <div className="container">

          <a className="navbar-brand" href="#"><img className="logo" src={logo} alt="logo..."></img></a>
          <button className="btn-lg btn-dark btn-block" id="btnsalida">log out</button>

        <div className="collapse navbar-collapse"  id="navbarSupportedContent">
          <ul className="navbar-nav m-auto"></ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar;
