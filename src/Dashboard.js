import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.scss';
import React, { Component } from "react";
import Sidebar from './components/Sidebar';
import Navbar from './components/Navbar';
import Ventas from './pages/Ventas';
import Cuenta from './pages/Cuenta';
import Clientes from './pages/Clientes';
import Empleados from './pages/Empleados';
import Menus from './pages/Menus'


class App extends React.Component {
    render(){
    return (
        <Router>
        <Navbar/>
        <div className="flex">
          <Sidebar />
          <div className="content">
            <Route path="/" exact={true} component={Cuenta} />

            <Route path="/ventas" exact={true} component={Ventas} />

            <Route path="/menus" exact={true} component={Menus} />

            <Route path="/clientes" exact={true} component={Clientes} />

            <Route path="/empleados" exact={true} component={Empleados} />
          </div>
        </div>
      </Router>
    );
    }
}

export default App;
